
function showAirports () {
   var country = document.getElementById("countryName").value;

   var req = new XMLHttpRequest();
   var uri = "airport_names.php?countryName="+country;
    req.open('GET', uri, /*async*/true);
    req.onreadystatechange = function() {
      if (req.readyState == 4 && req.status == 200) {
        var city_list = JSON.parse(req.responseText);
        var html_list = "";
        for (var i = 0; i < city_list.length; i++) {
            html_list +=  "<p><a href=\"javascript:showAirportInfo('"+city_list[i].code+"');\">"+city_list[i].name+" ("+city_list[i].code+")</a></p>";
        };
        document.getElementById("left").innerHTML = html_list;
        document.getElementById("right").innerHTML = "<p>&#8612; Select an airport from the left menu</p>";
      }
    };
    req.send(/*no params*/null);
    document.getElementById("left").innerHTML = mock_text;
};


function showAirportInfo (code) {
    var uri = "airport_info.php?airportCode="+encodeURI(code);
    var req = new XMLHttpRequest();
    req.open('GET', uri, /*async*/true);
    req.onreadystatechange = function() {
      if (req.readyState == 4 && req.status == 200) {
        var info = JSON.parse(req.responseText);
        show = "<h2>Airport of "+info.CityOrAirportName+" ("+info.AirportCode+")</h2>";
        show += "<ul><li>Runway Length: <strong>"+info.RunwayLengthFeet+ "</strong> feet</li>";
        show += "<li>Runway Elevation: <strong>"+info.RunwayElevationFeet+ "</strong> feet</li>";
        coordinates = info.LatitudeDegree+"&deg;"+info.LatitudeMinute+"'"+info.LatitudeSecond+"'' "+info.LatitudeNpeerS+", ";
        coordinates += info.LongitudeDegree+"&deg;"+info.LongitudeMinute+"'"+info.LongitudeSeconds+"'' "+info.LongitudeEperW;
        show += "<li>Coordinates: <strong>"+coordinates+ "</strong></li>";
        show += "</ul>";
        document.getElementById("right").innerHTML= show;
      }
    };
    req.send(/*no params*/null);
    document.getElementById("right").innerHTML="Processing request ...";
   
}

window.onload = showAirports();